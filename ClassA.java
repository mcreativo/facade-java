public class ClassA
{
    private String a;

    public ClassA(String a)
    {
        this.a = a;
    }

    public void doA()
    {
        a += " doing A";
    }

    public String getA()
    {
        return a;
    }
}
