public class ClassB
{
    private String b;

    public void doB()
    {
        b += " doing B ";
    }

    public String getB()
    {
        return b;
    }

    public void setB(String b)
    {
        this.b = b;
    }
}
