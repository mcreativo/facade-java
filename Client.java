public class Client
{
    public static void main(String[] args)
    {
        ClassA a = new ClassA("something");
        ClassB b = new ClassB();
        ClassC c = new ClassC();

        a.doA();
        b.setB(a.getA());
        b.doB();
        c.doC(b);

        String desirableObject = c.getC();
    }
}
