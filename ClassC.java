public class ClassC
{
    private String c;

    public void doC(ClassB b)
    {
        c = b.getB() + " doing C with B";
    }

    public String getC()
    {
        return c;
    }
}
